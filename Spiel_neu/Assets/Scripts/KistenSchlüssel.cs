﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KistenSchlüssel : MonoBehaviour

{

    public AudioSource audio;



    void OnTriggerEnter(Collider Other)
    {
        if (Other.tag == "Player")
        {
            Destroy(gameObject);
            audio.Play();
        }
    }


    // Use this for initialization
    void Start()
    {

        audio = GetComponent<AudioSource>();

    }


    // Update is called once per frame
    void Update()
    {

    }
}

