﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpielerNah : MonoBehaviour
{


    public bool berührt;

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            berührt = true;

        }

    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            berührt = false;
        }
    }
}
