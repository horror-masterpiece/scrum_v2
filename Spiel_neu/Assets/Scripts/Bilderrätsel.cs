﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bilderrätsel : MonoBehaviour {
    public GameObject bilder;


    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {

    }
    void OnTriggerEnter(Collider Other)
    {
        if (Other.tag == "Player")
        {
            bilder.transform.Translate(0f, 0.5f, 0f);
        }
    }
}