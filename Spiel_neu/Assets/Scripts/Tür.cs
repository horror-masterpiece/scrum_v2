﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tür : MonoBehaviour
{

    public bool türoffen;
    public string schlüssel;


    void Start()
    {

    }

    void Update()
    {


        if (türoffen == true)
        {

            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<MeshCollider>().enabled = false;

        }

        if (türoffen == false)

        {

            GetComponent<MeshRenderer>().enabled = true;
            GetComponent<MeshCollider>().enabled = true;


        }
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            for (int i = 0; i < GameManager.instance.inventar.Count; i++)
            {
                if (GameManager.instance.inventar[i].tag == schlüssel)
                {
                    türoffen = true;
                    GameManager.instance.inventar.RemoveAt(i);

                }

            }


        }
    }
}




