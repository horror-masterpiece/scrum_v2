﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tueroeffnen : MonoBehaviour {

    public Transform player;
    bool offen = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        if (entfernung <= 22f)
        {
            if (GameObject.Find("Schiebeschloss").GetComponent<Schiebeschloss>().geöffnet)
            {

                if (Input.GetButtonDown("Fire2"))
                {
                    GetComponent<MeshRenderer>().enabled = false;
                    GetComponent<BoxCollider>().enabled = false;

                    offen = true;
                }

            }
        }

        if (offen)
        {
            this.transform.Translate(-219.3f, -42.6f, -2.8f);
        }
    }
}
