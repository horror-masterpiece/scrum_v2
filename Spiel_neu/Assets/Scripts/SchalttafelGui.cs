﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SchalttafelGui : MonoBehaviour
{

    public GameObject knopf;
    public Button e;
    public GameObject codezeile;
    public InputField eingabe;
    public GameObject tuer;
    public bool gelöst;
    public string lösung;

    // Use this for initialization
    void Start()
    {

        knopf.SetActive(false);
        codezeile.SetActive(false);
        gelöst = false;

    }

    // Update is called once per frame
    void Update()
    {

        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        e.transform.position = pos;
        eingabe.transform.position = pos;

        if(gelöst == true)
        {
            knopf.SetActive(false);
            codezeile.SetActive(false);
        }

    }

    //Knopf_sichtbar
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            knopf.SetActive(true);
        }
    }

    //Codezeile_sichtbar
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetButtonDown("Fire2"))//InputManager!
            {
                knopf.SetActive(false);
                codezeile.SetActive(true);
                eingabe.ActivateInputField();
            }


            if (eingabe.text == lösung)
            {
                tuer.SetActive(false);
                gelöst = true;
             
            }
        }
    }



    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            knopf.SetActive(false);
            codezeile.SetActive(false);
        }
    }
}
