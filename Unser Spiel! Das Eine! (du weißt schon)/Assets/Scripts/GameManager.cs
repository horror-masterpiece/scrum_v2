﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    public int health;
    public int maxHealth;
    //public RectTransform healthBalken;
    public List<GameObject> inventar = new List<GameObject>();
    public Text Inventar;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void OnGUI()
    {
        
        Inventar.text = "" + "\n";

        for (int i = 0; i < inventar.Count; i++)
        { 
            Inventar.text += inventar[i].name + System.Environment.NewLine;
        }
     }


    // Use this for initialization
    void Start()
    {
        health = maxHealth;
        Scene scene = SceneManager.GetActiveScene();
        Debug.Log("Active scene is 0" + scene.name + "'.");
    }
    public void Sterben()
    {
        Debug.Log("Spieler ist gestorben");
        health = maxHealth;
        SceneManager.LoadScene("Level2", LoadSceneMode.Additive);

    }

    // Update is called once per frame
    void Update()
    {

        if (health >= maxHealth)
        {
            health = maxHealth;

        }
    }
}

