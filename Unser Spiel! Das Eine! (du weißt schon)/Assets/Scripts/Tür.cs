﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tür : MonoBehaviour
{

    public bool türoffen = false;
    public string schlüssel;
    public Transform player;


    void Start()
    {

    }

    void Update()
    {
        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        if (entfernung <= 20f)
        {
            if (Input.GetButtonDown("Fire2"))
            {

                for (int i = 0; i < GameManager.instance.inventar.Count; i++)
                {
                    if (GameManager.instance.inventar[i].tag == schlüssel)
                    {
                        türoffen = true;
                        GameManager.instance.inventar.RemoveAt(i);
                    }
                }
            }
        }


        if (türoffen == true)
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
        }

    }

}




