﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bilderrätsel : MonoBehaviour {

    public GameObject knopf;
    public Image kreis;
    public bool verschoben = false;



    // Use this for initialization
    void Start() {

        knopf.SetActive(false);
        
    }

    // Update is called once per frame
    void Update() {
        
        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        kreis.transform.position = pos;

        if (verschoben)
        {
            knopf.SetActive(false);
        }

    }
    void OnTriggerEnter(Collider Other)
    {
        if (Other.tag == "Player")
        {
            knopf.SetActive(true);
        }
    }


    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetButtonDown("Fire2"))
            {
                knopf.SetActive(false);
                transform.localPosition = new Vector3 (88.1f, -99.3f, -93.53f);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            knopf.SetActive(false);
        }
    }
}