﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Schiebeschloss : MonoBehaviour {

    public bool geöffnet;
    public bool spielernah;

	// Use this for initialization
	void Start () {

        geöffnet = false;
		
	}
	
	// Update is called once per frame
	void Update () {

        if (spielernah){

            if (Input.GetButton ("Fire2"))
            {
                transform.localPosition = new Vector3(-217, -50, 10);
                geöffnet = true;
            }
        }
		
	}

    void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            spielernah = true;
            
        }
        else
        {
            spielernah = false;
        }
    }
}
