﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Aufzugknopf : MonoBehaviour {

    public bool gedrückt = false;

    public GameObject knopf;
    public Image kreis;

	// Use this for initialization
	void Start () {

        knopf.SetActive(false);
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        kreis.transform.position = pos;

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            knopf.SetActive(true);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetButtonDown("Fire2"))
            {
                knopf.SetActive(false);
                gedrückt = true;
                
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            knopf.SetActive(false);
        }
    }
}
