﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterVerhalten : MonoBehaviour
{

    public GameObject[] waypoints;
    NavMeshAgent agent;
    int currentwp;
    public Transform player;
    public bool entdeckung;
    public bool entkommen = false;
    bool getroffen;
    public Transform sicht;
    RaycastHit hit;
    SpielerNah nahscript;
    public float limit;
    public float countdown;

    //public AudioSource audio;


    // Use this for initialization
    void Start()
    {

        nahscript = GameObject.Find("Berührung").GetComponent<SpielerNah>();

        //Patroullieren
        agent = this.GetComponent<NavMeshAgent>();
        currentwp = Random.Range(0, waypoints.Length);
        agent.SetDestination(waypoints[currentwp].transform.position);


        entdeckung = false;
        countdown = limit;

        //audio = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {

        //Sichtwinkel
        Vector3 direction = player.position - this.transform.position;
        direction.y = 0;
        float angle = Vector3.Angle(direction, sicht.forward);



        //Patroullieren
        if (agent.remainingDistance < 0.5)
        {
            currentwp = Random.Range(0, waypoints.Length);
            agent.SetDestination(waypoints[currentwp].transform.position);
        }
        else if (agent.hasPath)
        {
            Vector3 toTarget = agent.steeringTarget - this.transform.position;
            float turnAngle = Vector3.Angle(this.transform.forward, toTarget);
            agent.acceleration = turnAngle * agent.speed;
        }


        //Spielerentdeckung
        if (Vector3.Distance(player.position, this.transform.position) < 150 && angle < 50 || entdeckung || nahscript.berührt)
        {


            //Raycast
            float distance = Vector3.Distance(this.transform.position, player.position);
            Physics.Raycast(this.transform.position, direction, distance);
            getroffen = Physics.Raycast(this.transform.position, direction, out hit);


            if (getroffen == true && hit.collider.tag == "Player")
            {
                entdeckung = true;
                entkommen = false;
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);

                //Verfolgen
                agent.SetDestination(player.position);


                //Töten
                GameManager.instance.health = GameManager.instance.health - 1;

                if (GameManager.instance.health <= 0)
                {
                    Debug.Log("Gestorben!");
                    //GameManager.instance.Sterben();
                }

            }
        }
        else
        {
            entdeckung = false;

        }


        //Fliehen
        if (entdeckung == true)
        {

            if (hit.collider.tag == "Untagged" || direction.magnitude > 200)
            {
                agent.SetDestination(player.position);
                countdown -= Time.deltaTime;
                GameManager.instance.health = GameManager.instance.health;

                if (countdown <= 0)
                {
                    entdeckung = false;
                    entkommen = true;
                    countdown = limit;
                    agent.SetDestination(waypoints[currentwp].transform.position);


                }
            }
        }

        if (entkommen)
        {
            GameManager.instance.health = GameManager.instance.health + 4;
        }

    }

}
