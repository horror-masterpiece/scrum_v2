﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aufzug : MonoBehaviour {

    public GameObject knopf;
    public GameObject player;
    public Transform target;
    public float speed;
    public bool spielerdrin = false;


    // Use this for initialization
    void Start()
    {

    }
	// Update is called once per frame
	void Update () {

        float step = speed * Time.deltaTime;


        if (knopf.GetComponent<Aufzugknopf>().gedrückt && spielerdrin)
        {
            player.transform.parent = transform;
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);
                  
        }

        if (!spielerdrin)
        {
            player.transform.parent = null;
        }

	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            spielerdrin = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            spielerdrin = false;
        }
    }
}
