﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    public float moveSpeed;
    public float jumpSpeed;
    public float schleichen;


    bool isGrounded;
    public bool geduckt = false;
    //public Transform kamera;

    public float speed;
    //public float limit;
    //public float countdown;

    // Use this for initialization
    void Start()
    {
        //countdown = limit;
    }

    // Update is called once per frame
    void Update()
    {

        speed = GetComponent<Rigidbody>().velocity.magnitude;

        //Grounded
        isGrounded = Physics.Raycast(transform.position, -transform.up, transform.localScale.y + 1f);


        //Jump
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
        }


        //Ducken
        if (Input.GetButtonDown("Fire3"))
        {
            GetComponent<Transform>().localScale = new Vector3(8, 4f, 8);
            GetComponent<CapsuleCollider>().height = 1;

            //kamera.transform.Translate(-0.04f, 0, -0.04f);
            geduckt = true;
        }

        else if (Input.GetButtonUp("Fire3"))
        {
            GetComponent<Transform>().localScale = new Vector3(8, 8, 8);
            GetComponent<CapsuleCollider>().height = 2;

            //kamera.transform.Translate(-0.04f, 0.53f, -0.04f);
            geduckt = false;
        }

        //if (countdown <= 0)
        //{
            //GetComponent<CapsuleCollider>().height = 2;
            //countdown = limit;
        //}

        //if (!geduckt)
        //{
            //countdown -= Time.deltaTime;
        //}

        if (geduckt)
        {
            jumpSpeed = 0;
            moveSpeed = schleichen;
        }
        else
        {
            jumpSpeed = 50;
            moveSpeed = 20;
        }

        //Movement WASD
        float h = Input.GetAxis("Horizontal") * moveSpeed;
        float v = Input.GetAxis("Vertical") * moveSpeed;
        h *= Time.deltaTime;
        v *= Time.deltaTime;

        transform.Translate(h, 0, v);

    }

    //void OnDrawGizmos()
    //{
      //Gizmos.color = Color.red;
      //Gizmos.DrawLine(transform.position, transform.position + -transform.up * 1f);
    //}

}