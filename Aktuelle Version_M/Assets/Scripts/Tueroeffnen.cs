﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tueroeffnen : MonoBehaviour {

    public Transform player;
    bool offen = false;
    public GameObject tuer;


	void Update () {

        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        if (entfernung <= 22f)
        {
            if (GameObject.Find("Schiebeschloss").GetComponent<Schiebeschloss>().geöffnet)
            {

                if (Input.GetButtonDown("Fire2"))
                {
                    tuer.GetComponent<MeshRenderer>().enabled = false;
                    tuer.GetComponent<MeshCollider>().enabled = false;

                    offen = true;
                }

            }
        }

        if (offen)
        {
            this.transform.Translate(-219.3f, -42.6f, -2.8f);
        }
    }
}
