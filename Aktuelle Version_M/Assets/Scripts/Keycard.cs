﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Keycard : MonoBehaviour {

    public GameObject uniform;
    public GameObject kurbel;
    public GameObject kreis;
    public Image punkt;
    public Transform player;
    public bool aufgesammelt = false;


    void Start () {

        uniform.GetComponent<MeshRenderer>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        kreis.SetActive(false);
    }
	

	void Update () {

        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        punkt.transform.position = pos;

        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        if (kurbel.GetComponent<Kurbel>().uni)
        {
            uniform.GetComponent<MeshRenderer>().enabled = true;
            GetComponent<MeshRenderer>().enabled = true;

        }

        if (kurbel.GetComponent<Kurbel>().gelöst && aufgesammelt == false)
        {
            if (entfernung <= 10)
            {
                kreis.SetActive(true);

                if (Input.GetButtonDown("Fire2"))
                {
                    GetComponent<MeshRenderer>().enabled = false;
                    GameManager.instance.inventar.Add(gameObject);
                    aufgesammelt = true;
                }
            }
            else
            {
                kreis.SetActive(false);
            }
        }

        if (aufgesammelt)
        {
            kreis.SetActive(false);
            GetComponent<MeshRenderer>().enabled = false;
        }
		
	}
}
