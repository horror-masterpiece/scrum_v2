﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pausenmenue : MonoBehaviour {

    public GameObject button1;
    public GameObject button2;
    public bool aktiviert = false;
    public GameObject kamera;


	void Start () {

        button1.SetActive(false);
        button2.SetActive(false);
    }
	

	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            button1.SetActive(true);
            button2.SetActive(true);
            aktiviert = true;
            kamera.GetComponent<CameraMove>().enabled = false;
        }

	}

    public void Fortsetzen()
    {
        kamera.GetComponent<CameraMove>().enabled = true;
        button1.SetActive(false);
        button2.SetActive(false);
        aktiviert = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Hauptmenü(string GameLevel)
    {
        SceneManager.LoadScene(GameLevel);
    }
}
