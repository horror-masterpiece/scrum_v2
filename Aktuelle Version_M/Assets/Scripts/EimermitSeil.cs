﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EimermitSeil : MonoBehaviour
{
    public Transform player;
    public GameObject seil;
    public bool kombiniert = false;
    public GameObject combine;
    public Text kombi;
    public GameObject kreis;
    public Image punkt;
    public bool eingesammelt = false;
    public float limit;
    public float countdown;
    bool zeit = false;


    void Start()
    {
        seil.SetActive(false);
        combine.SetActive(false);
        kreis.SetActive(false);
        countdown = limit;
    }


    void Update()
    {
        //GUI
        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        kombi.transform.position = pos;
        punkt.transform.position = pos;


        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        if(entfernung <= 10)
        {
            for (int i = 0; i < GameManager.instance.inventar.Count; i++)
            {
                if (GameManager.instance.inventar[i].tag == "Seil")
                {
                    combine.SetActive(true);

                    if (Input.GetButtonDown("Fire2"))
                    {
                        combine.SetActive(false);
                        seil.SetActive(true);
                        kombiniert = true;
                        GameManager.instance.inventar.RemoveAt(i);
                        zeit = true;
                        
                    }
                }
            }
        }
        else
        {
            combine.SetActive(false);
        }

        if (kombiniert)
        {
            combine.SetActive(false);

            if (countdown <= 0)
            {

                if (entfernung <= 10)
                {
                    kreis.SetActive(true);

                    if (Input.GetButtonDown("Fire2"))
                    {
                        kreis.SetActive(false);
                        eingesammelt = true;
                        GameManager.instance.inventar.Add(gameObject);
                        gameObject.SetActive(false);
                        zeit = false;
                    }
                }

            }
        }

        if (zeit)
        {
            countdown -= Time.deltaTime;
        }

    }
}