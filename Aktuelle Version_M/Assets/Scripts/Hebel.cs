﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hebel : MonoBehaviour
{

    public bool gehebelt = false;
    public GameObject intkreis;
    public Image kreis;
    public GameObject geheimtür;


    void Start()
    {

        intkreis.SetActive(false);

    }


    void Update()
    {

        //Gui
        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        kreis.transform.position = pos;

        if (gehebelt)
        {
            intkreis.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            intkreis.SetActive(true);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetButtonDown("Fire2"))
            {
                intkreis.SetActive(false);
                gehebelt = true;
                geheimtür.GetComponent<MeshCollider>().enabled = false;
                geheimtür.GetComponent<MeshRenderer>().enabled = false;
                GetComponent<Animator>().Play("Hebel");

            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            intkreis.SetActive(false);
        }
    }
}
