﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bildschlüssel : MonoBehaviour {

    public Transform player;
    public GameObject knopf;
    public Image kreis;
    public bool eingesammelt = false;
    public GameObject bild;


    void Start () {

        knopf.SetActive(false);

    }
	

	void Update () {

        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        kreis.transform.position = pos;

        if (bild.GetComponent<Bilderrätsel>().verschoben)
        {
            if(entfernung <= 13)
            {
                knopf.SetActive(true);

                if (Input.GetKeyDown(KeyCode.E))
                {
                    knopf.SetActive(false);
                    eingesammelt = true;
                    GameManager.instance.inventar.Add(gameObject);
                    gameObject.SetActive(false);
                }
            }
            else
            {
                knopf.SetActive(false);
            }
        }

        if (eingesammelt)
        {
            knopf.SetActive(false);
        }
		
	}
}
