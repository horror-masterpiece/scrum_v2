﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Schluesseloeffnen : MonoBehaviour {

    public GameObject schluessel;
    public bool geoeffnet = false;
    public Transform player;

	
	// Update is called once per frame
	void Update () {

        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        if (entfernung <= 20f)
        {

                if (schluessel.GetComponent<Einsammeln>().eingesammelt)
                {
                    if (Input.GetButtonDown("Fire2"))
                    {
                        GetComponent<MeshRenderer>().enabled = false;
                        GetComponent<BoxCollider>().enabled = false;

                        geoeffnet = true;
                    }
                }
        }

        if (geoeffnet)
        {
            this.transform.Translate(-63.8f, -46.1f, -75.2f);
        }

    }

    
}
