﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SchalttafelGui : MonoBehaviour
{

    public GameObject knopf;
    public Button e;
    public GameObject codezeile;
    public InputField eingabe;
    public GameObject tuer;
    public bool gelöst;
    public string lösung;
    AudioSource pieps;
    public AudioClip ton;
    bool soundplayed = false;
    bool sound = false;

    void Start()
    {

        knopf.SetActive(false);
        codezeile.SetActive(false);
        gelöst = false;

    }


    void Update()
    {

        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        e.transform.position = pos;
        eingabe.transform.position = pos;

        if(gelöst == true)
        {
            knopf.SetActive(false);
            codezeile.SetActive(false);
        }

        if (sound && !soundplayed)
        {
            //Audio
            pieps = GetComponent<AudioSource>();
            pieps.PlayOneShot(ton, 0.3f);
            soundplayed = true;
        }
    }

    //Knopf_sichtbar
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            knopf.SetActive(true);
        }
    }

    //Codezeile_sichtbar
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetButtonDown("Fire2"))//InputManager!
            {
                knopf.SetActive(false);
                codezeile.SetActive(true);
                eingabe.ActivateInputField();
            }


            if (eingabe.text == lösung)
            {
                tuer.SetActive(false);
                gelöst = true;
                sound = true;
            }
        }
    }



    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            knopf.SetActive(false);
            codezeile.SetActive(false);
        }
    }
}
