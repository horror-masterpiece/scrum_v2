﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Kurbel : MonoBehaviour {

    public Transform player;
    public GameObject kreis;
    public Image punkt;
    public GameObject kurbeldreh;
    public bool runter = false;
    public bool hoch = false;
    public float limit;
    public float countdown;
    bool ani = false;
    bool timer = false;
    bool timer2 = false;
    public GameObject eimer;
    public GameObject seil;
    public float countdown2;
    public bool gelöst = false;
    public bool uni = false;


    void Start () {

        kreis.SetActive(false);
        countdown = limit;
        countdown2 = limit;
		
	}
	

	void Update () {

        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        punkt.transform.position = pos;

        if(GameObject.Find("Eimer am Brunnen").GetComponent<Brunneneimer>().aufgehängt && runter == false && gelöst == false)
        {
            if(entfernung <= 7)
            {
                kreis.SetActive(true);

                if (Input.GetButtonDown("Fire2"))
                {
                    timer = true;
                    kurbeldreh.GetComponent<Animator>().Play("Kurbel");
                    eimer.GetComponent<Animator>().Play("Eimer");
                    seil.GetComponent<Animator>().Play("Seil am Brunnen");
                    ani = true;
                }
            }
            else
            {
                kreis.SetActive(false);
            }
        }

        if(countdown <= 0)
        {
            runter = true;
            ani = false;
            timer = false;
        }

        if (ani)
        {
            kreis.SetActive(false);
        }

        if (timer)
        {
            countdown -= Time.deltaTime;
        }

        if (runter)
        {
            if (entfernung <= 7)
            {

                if (Input.GetButtonDown("Fire2"))
                {
                   
                    kurbeldreh.GetComponent<Animator>().Play("Kurbel 2");
                    eimer.GetComponent<Animator>().Play("Eimer 2");
                    seil.GetComponent<Animator>().Play("Seil am Brunnen 2");
                    timer2 = true;
                    kreis.SetActive(false);
                    
                    uni = true;

                }
            }
            else
            {
                kreis.SetActive(false);
            }
        }

        if(entfernung <= 7 && runter)
        {
            kreis.SetActive(true);
        }

        if (timer2)
        {
            countdown2 -= Time.deltaTime;
            ani = true;
        }
        
        if(countdown2 <= 0)
        {
            hoch = true;
            timer2 = false;
            runter = false;
            gelöst = true;
        }

        if (hoch)
        {
            kreis.SetActive(false);
        }

    }
}
