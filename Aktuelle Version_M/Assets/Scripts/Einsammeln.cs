﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Einsammeln : MonoBehaviour {

    public bool eingesammelt = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter (Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.instance.inventar.Add(gameObject);
            gameObject.SetActive(false);

            eingesammelt = true;

        }
    }
}
