﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hebeltür : MonoBehaviour
{
    public GameObject tuer;
    public GameObject hebel;
    public bool gelöst;

    // Use this for initialization
    void Start()
    {
        gelöst = false;

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider Other)
    {
        if (Other.tag == "Player")
        {
            hebel.transform.Translate(0f, -0.2f, 0f);
            tuer.SetActive(false);
            gelöst = true;

        }
    }
}
