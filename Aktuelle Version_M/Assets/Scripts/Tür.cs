﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tür : MonoBehaviour
{

    public bool türoffen = false;
    public string schlüssel;
    public Transform player;
    public GameObject tuer;


    void Update()
    {
        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        if (entfernung <= 20f)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {

                for (int i = 0; i < GameManager.instance.inventar.Count; i++)
                {
                    if (GameManager.instance.inventar[i].tag == schlüssel)
                    {
                        türoffen = true;
                        GameManager.instance.inventar.RemoveAt(i);
                       
                    }
                }
            }
        }


        if (türoffen == true)
        {
            tuer.GetComponent<MeshRenderer>().enabled = false;
            tuer.GetComponent<MeshCollider>().enabled = false;
        }

    }

}




