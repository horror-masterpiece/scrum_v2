﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeinenSound : MonoBehaviour {

    private AudioSource girl;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            girl = GetComponent<AudioSource>();
            girl.Play();
            GetComponent<BoxCollider>().enabled = false;
        }
    }
}
