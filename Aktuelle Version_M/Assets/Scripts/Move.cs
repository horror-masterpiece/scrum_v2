﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    public float moveSpeed;
    public float jumpSpeed;
    public float schleichen;

    bool isGrounded;
    public bool geduckt = false;

    public float speed;
    AudioSource schritte;
    Vector3 lastPosition;
    float zeit;

    //Mauscursor
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        //Schritte
        speed = (this.transform.position - this.lastPosition).magnitude / (Time.time - zeit);
        this.lastPosition = transform.position;
        zeit = Time.time;

       
        Schritte();
       

        //Grounded
        isGrounded = Physics.Raycast(transform.position, -transform.up, transform.localScale.y + 1f);


        //Jump
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
        }


        //Ducken
        if (Input.GetButtonDown("Fire3") && !geduckt)
        {
            geduckt = true;
            GetComponent<CapsuleCollider>().center = new Vector3(0, -0.3675447f, 0);
            GetComponent<CapsuleCollider>().height = 1.264911f;
        }

        else if (Input.GetButtonDown("Fire3") && geduckt)
        {
            geduckt = false;
            GetComponent<CapsuleCollider>().center = new Vector3(0, 0, 0);
            GetComponent<CapsuleCollider>().height = 2;
        }

        if (geduckt)
        {
            jumpSpeed = 0;
            moveSpeed = schleichen;
        }
        else
        {
            jumpSpeed = 30;
            moveSpeed = 16;
        }


        //Movement WASD
        float h = Input.GetAxis("Horizontal") * moveSpeed;
        float v = Input.GetAxis("Vertical") * moveSpeed;
        h *= Time.deltaTime;
        v *= Time.deltaTime;

        transform.Translate(h, 0, v);

        //Mauscursor
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }

    }

    //Audio
    void Schritte()
    {
        schritte = GetComponent<AudioSource>();

        if( speed > 0.1f)
        {
            schritte.enabled = true;
            schritte.loop = true;
        }
        if(speed < 0.1f)
        {
            schritte.enabled = false;
            schritte.loop = false;
        }
        if (GameObject.Find("Hochknopf").GetComponent<Aufzugknopf>().gedrückt && GameObject.Find("Aufzugganz").GetComponent<Aufzug>().spielerdrin)
        {
            schritte.enabled = false;
            schritte.loop = false;
        }
        if(GameManager.instance.countdown < GameManager.instance.limit)
        {
            schritte.enabled = false;
            schritte.loop = false;
        }
    }


    //void OnDrawGizmos()
    //{
    //Gizmos.color = Color.red;
    //Gizmos.DrawLine(transform.position, transform.position + -transform.up * 1f);
    //}

}