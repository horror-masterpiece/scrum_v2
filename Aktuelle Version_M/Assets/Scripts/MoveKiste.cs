﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveKiste : MonoBehaviour {


    public GameObject knopfi;
    public Image kreis;
    public bool hochgehoben;
    public Transform player;
    public Transform playercam;
    public bool spielernah;
    bool berührt;

    RaycastHit hit;
    bool getroffen;
   

	void Start () {

        knopfi.SetActive(false);

        hochgehoben = false;
        spielernah = false;
        berührt = false;

    }
	
	
	void Update () {

        //GUI
        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        kreis.transform.position = pos;

        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        if(entfernung <= 22f)
        {
            
            //Raycast
            Physics.Raycast(playercam.transform.position, playercam.transform.forward, 22f);
            getroffen = Physics.Raycast(playercam.transform.position, playercam.transform.forward, out hit);

            if(getroffen && hit.collider.name == gameObject.transform.name)
            {
                knopfi.SetActive(true);
                spielernah = true;
            }
            else
            {
                spielernah = false;
            }

        }
        else
        {
            spielernah = false;
            knopfi.SetActive(false);
        }

        if(spielernah && Input.GetMouseButtonDown(0))
        {
            GetComponent<Rigidbody>().isKinematic = true;
            transform.parent = playercam;
            hochgehoben = true;
        }

        if (hochgehoben)
        {
            knopfi.SetActive(false);

            if (Input.GetMouseButtonUp(0))
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;

                hochgehoben = false;
            }

            if (berührt)
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                hochgehoben = false;
                berührt = false;
            }
        }
    }

    void OnTriggerEnter()
    {
        if (hochgehoben)
        {
            berührt = true;
        }
    }

    //void OnDrawGizmos()
    //{
      // Gizmos.color = Color.red;

        //Gizmos.DrawLine(playercam.transform.position, playercam.transform.position + playercam.transform.forward * 22f);
    //}
}
