﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bilderrätsel : MonoBehaviour {

    public GameObject knopf;
    public Image kreis;
    public bool verschoben = false;
    public float limit;
    public float countdown;
    bool ani = false;



    void Start() {

        knopf.SetActive(false);
        countdown = limit;
        
    }


    void Update() {
        
        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        kreis.transform.position = pos;

        if (verschoben)
        {
            knopf.SetActive(false);
        }

        if (countdown <= 0)
        {
            verschoben = true;
        }

        if (ani)
        {
            countdown -= Time.deltaTime;
        }

        if(countdown <= -3)
        {
            ani = false;
        }


    }


    void OnTriggerEnter(Collider Other)
    {
        if (Other.tag == "Player")
        {
            knopf.SetActive(true);
        }
    }


    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                knopf.SetActive(false);
                GetComponent<Animator>().Play("Bild");
                ani = true;
                
                
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            knopf.SetActive(false);
        }
    }
}