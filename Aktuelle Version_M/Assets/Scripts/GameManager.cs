﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    public int health;
    public int maxHealth;
    public List<GameObject> inventar = new List<GameObject>();
    public Text Inventar;
    public float limit;
    public float countdown;
    public GameObject todesbild;
    public GameObject todestext;
    public GameObject player;
    public GameObject doppeltür;
    public GameObject monsterOG;
    public GameObject monsterEG;
    public GameObject cp1;
    public GameObject cp2;
    AudioSource spielertod;
    public AudioClip sterben;
    public bool sound = false;
    bool soundplayed = false;
    float limit2 = 3f;
    public float countdown2;
    bool abklingzeit = false;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void OnGUI()
    {
        Inventar.text = "" + "\n";

        for (int i = 0; i < inventar.Count; i++)
        {
            Inventar.text += inventar[i].name + System.Environment.NewLine;
        }
    }


    void Start()
    {
        health = maxHealth;
        Scene scene = SceneManager.GetActiveScene();
        Debug.Log("Active scene is 0" + scene.name + "'.");
        countdown = limit;
        todesbild.SetActive(false);
        todestext.SetActive(false);

        monsterOG.GetComponent<MonsterWP2>().enabled = false;
        monsterOG.SetActive(false);
        monsterEG.SetActive(false);

        countdown2 = limit2;

    }

    public void Sterben()
    {

        countdown -= Time.deltaTime;
        todesbild.GetComponent<Animator>().Play("Todbild");
        todestext.GetComponent<Animator>().Play("Todtext");
        todesbild.SetActive(true);
        todestext.SetActive(true);

        sound = true;

        player.GetComponent<Move>().enabled = false;

        countdown2 = limit2;
        abklingzeit = false;

        if (countdown <= 0)
        {
            health = maxHealth;
            GameObject.FindWithTag("Checkpoint").GetComponent<Checkpoint>().Load();
            countdown = limit;
            todesbild.GetComponent<Animator>().Play("New State");
            todestext.GetComponent<Animator>().Play("New State");

            player.GetComponent<Move>().enabled = true;

            abklingzeit = true;
            
        }
    }

    void Update()
    {

        if (health >= maxHealth)
        {
            health = maxHealth;

        }


        //KIWP
        if (doppeltür.GetComponent<Doppeltüre>().türoffen)
        {
            monsterOG.GetComponent<MonsterWP2>().enabled = true;
            monsterOG.GetComponent<MonsterVerhalten>().enabled = false;
        }

        if (cp1.GetComponent<Checkpoint>().aktiviert)
        {
            monsterEG.SetActive(true);
        }
        if (cp2.GetComponent<Checkpoint>().aktiviert)
        {
            monsterEG.SetActive(false);
            monsterOG.SetActive(true);
        }

        //Gui
        if(health <= 400)
        {
            GameObject.Find("Verletzt").GetComponent<Animator>().Play("Verletzung");
        }
        if(health == maxHealth)
        {
            GameObject.Find("Verletzt").GetComponent<Animator>().Play("New State");
        }


        //Audio
        if(sound && !soundplayed)
        {
            spielertod = GetComponent<AudioSource>();
            spielertod.PlayOneShot(sterben, 1f);
            soundplayed = true;
        }
        if (abklingzeit)
        {
            countdown2 -= Time.deltaTime;
            
        }
        if(countdown2 <= 0)
        {
            soundplayed = false;
            sound = false;
        }
        if(countdown2 <= -5)
        {
            abklingzeit = false;
            countdown2 = limit2;
        }
    }
}

