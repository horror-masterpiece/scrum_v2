﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brunneneimer : MonoBehaviour {

    public Transform player;
    public GameObject kreis;
    public Image punkt;
    public GameObject eimer;
    public bool aufgehängt = false;
    public GameObject eimer2;
    public GameObject seil1;
    public GameObject seil2;


    void Start()
    {

        kreis.SetActive(false);
        eimer2.GetComponent<MeshRenderer>().enabled = false;
        seil1.GetComponent<MeshRenderer>().enabled = false;
        seil2.GetComponent<MeshRenderer>().enabled = false;
    }


    void Update()
    {

        float entfernung = Vector3.Distance(gameObject.transform.position, player.position);

        Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
        punkt.transform.position = pos;

        for (int i = 0; i < GameManager.instance.inventar.Count; i++)
        {
            if (GameManager.instance.inventar[i].tag == "Eimer")
            {
                if (entfernung <= 15)
                {
                    kreis.SetActive(true);

                    if (Input.GetButtonDown("Fire2"))
                    {
                        eimer2.GetComponent<MeshRenderer>().enabled = true;
                        seil1.GetComponent<MeshRenderer>().enabled = true;
                        seil2.GetComponent<MeshRenderer>().enabled = true;

                        aufgehängt = true;
                        kreis.SetActive(false);
                        GameManager.instance.inventar.RemoveAt(i);
                    }
                }
                else
                {
                    kreis.SetActive(false);
                }

                if (aufgehängt)
                {
                    kreis.SetActive(false);
                }
            }
        }
    }

    
}
