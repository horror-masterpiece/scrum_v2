﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    Vector2 cameraLook;
    Vector2 smooth;
    public float sensitivity;
    public float smoothing;
    public GameObject player;


    void Update()
    {

        var move = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));


        move = Vector2.Scale(move, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
        smooth.x = Mathf.Lerp(smooth.x, move.x, 1f / smoothing);
        smooth.y = Mathf.Lerp(smooth.y, move.y, 1f / smoothing);
        cameraLook += smooth;

        transform.localRotation = Quaternion.AngleAxis(-cameraLook.y, Vector3.right);
        player.transform.localRotation = Quaternion.AngleAxis(cameraLook.x, player.transform.up);


        //Ducken
        if (GameObject.Find("Player").GetComponent<Move>().geduckt)
        {
            GetComponent<Animator>().Play("KameraDown");
        }

        if (GameObject.Find("Player").GetComponent<Move>().geduckt == false)
        {
            GetComponent<Animator>().Play("KameraUp");
        }
    }
}
