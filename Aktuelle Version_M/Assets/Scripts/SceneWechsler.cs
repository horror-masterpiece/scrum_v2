﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneWechsler : MonoBehaviour {

    public void SpielLaden(string GameLevel)
    {
        SceneManager.LoadScene(GameLevel);
    }

    public void SpielSchliessen()
    {
        Application.Quit();
    }
}
