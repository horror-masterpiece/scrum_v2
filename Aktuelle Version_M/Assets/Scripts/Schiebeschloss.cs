﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Schiebeschloss : MonoBehaviour {

    public bool geöffnet;
    public bool spielernah;
    public GameObject balken;
    public float limit;
    public float countdown;
    bool ani = false;


	void Start () {

        geöffnet = false;
        countdown = limit;
		
	}
	

	void Update () {

        if (spielernah){

            if (Input.GetButton ("Fire2"))
            {
                balken.GetComponent<Animator>().Play("Schiebeschloss");
                ani = true;
            }
        }

        if (ani)
        {
            countdown -= Time.deltaTime;
        }

        if (countdown <= 0)
        {
            geöffnet = true;
        }

        if (countdown <= -3)
        {
            ani = false;
        }

    }

    void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            spielernah = true;
            
        }
        else
        {
            spielernah = false;
        }
    }
}
