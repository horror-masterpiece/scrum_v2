﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TürohneSchlüssel : MonoBehaviour {

    public GameObject tuer;


    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetButtonDown("Fire2"))
            {

                GetComponent<BoxCollider>().enabled = false;
                //GetComponent<MeshRenderer>().enabled = false;
                tuer.GetComponent<MeshCollider>().enabled = false;
                tuer.GetComponent<MeshRenderer>().enabled = false;

            }
        }
    }
}
