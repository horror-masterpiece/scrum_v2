﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallSchaden : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameManager.instance.Sterben();
        }
    }
}
