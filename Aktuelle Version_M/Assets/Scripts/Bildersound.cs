﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bildersound : MonoBehaviour {

    private AudioSource bilder;


    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            bilder = GetComponent<AudioSource>();
            bilder.Play();
            GetComponent<BoxCollider>().enabled = false;
        }
    }

}
