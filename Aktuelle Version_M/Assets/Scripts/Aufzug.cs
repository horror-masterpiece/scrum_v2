﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aufzug : MonoBehaviour {

    public GameObject knopf;
    public GameObject player;
    public Transform target;
    public float speed;
    public bool spielerdrin = false;
    AudioSource hochfahren;
    bool soundplayed = false;
    public AudioClip lift;



	void Update () {

        float step = speed * Time.deltaTime;


        if (knopf.GetComponent<Aufzugknopf>().gedrückt && spielerdrin)
        {
            player.transform.parent = transform;
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);

            //Audio
            if (!soundplayed)
            {
                hochfahren = GetComponent<AudioSource>();
                hochfahren.PlayOneShot(lift, 1f);
                soundplayed = true;
            }
                  
        }

        if (!spielerdrin)
        {
            player.transform.parent = null;
        }

	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            spielerdrin = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            spielerdrin = false;
        }
    }
}
