﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{

    public bool aktiviert;
    public Transform player;


    void Start()
    {

        aktiviert = false;

    }


    void Update()
    {

        //if (Input.GetKeyDown(KeyCode.C))
        //{
          //  Load();
            //player.transform.localRotation = Quaternion.identity;
        //}
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            aktiviert = true;
            Save();
            GetComponent<BoxCollider>().enabled = false;
            GameManager.instance.health = GameManager.instance.maxHealth;
        }
    }

    public void Save()
    {
        PlayerPrefs.SetFloat("CheckpointX", transform.position.x);
        PlayerPrefs.SetFloat("CheckpointY", transform.position.y);
        PlayerPrefs.SetFloat("CheckpointZ", transform.position.z);
    }

    public void Load()
    {
        float x = PlayerPrefs.GetFloat("CheckpointX");
        float y = PlayerPrefs.GetFloat("CheckpointY");
        float z = PlayerPrefs.GetFloat("CheckpointZ");

        player.position = new Vector3(x, y, z);

    }
}