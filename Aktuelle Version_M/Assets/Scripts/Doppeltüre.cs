﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doppeltüre : MonoBehaviour
{

    public bool türoffen = false;
    public string schlüssel;
    public GameObject tür1;
    public GameObject tür2;

    void Start()
    {

    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {

            for (int i = 0; i < GameManager.instance.inventar.Count; i++)
            {
                if (GameManager.instance.inventar[i].tag == schlüssel)
                {
                    if (Input.GetButtonDown("Fire2"))
                    {
                        türoffen = true;
                        GameManager.instance.inventar.RemoveAt(i);
                        tür1.SetActive(false);
                        tür2.SetActive(false);
                    }
                }
            }

        }
    }

}
