﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lesegeraet : MonoBehaviour {

    public GameObject tuer;
    public bool geöffnet = false;


    void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            for (int i = 0; i < GameManager.instance.inventar.Count; i++)
            {
                if (GameManager.instance.inventar[i].tag == "Schlüssel")
                {

                    if (Input.GetButtonDown("Fire2"))
                    {
                        tuer.GetComponent<MeshRenderer>().enabled = false;
                        tuer.GetComponent<MeshCollider>().enabled = false;
                        geöffnet = true;
                        GameManager.instance.inventar.RemoveAt(i);
                    }
                }
            }
        }
    }

}
